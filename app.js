const express = require("express");
const path = require("path");
const app = express();
const { createProxyMiddleware } = require("http-proxy-middleware");

app.use(express.static(path.join(__dirname, "build")));

app.use(
  "/services",
  createProxyMiddleware({
    target: "https://api.dev.sv-ai.cyclone-robotics.com/gcagrici/executor/",
    // target: "https://api.dev.bj-ai.cyclone-robotics.com/executor/executor/",
    secure: false,
    pathRewrite: {
      "^/services": "",
    },
    changeOrigin: true,
  })
);

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "build", "index.html"));
});

app.listen(8081, () => {
  console.log("success in port 8081");
});
